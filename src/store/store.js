import Vue from 'vue';
import Vuex from 'vuex';
import axios from '../axios';
import router from '../router'

Vue.use(Vuex);

function setLocalStorage(data) {
  localStorage.setItem('accessToken', data.accessToken);
  const userJSON = JSON.stringify(data.user);
  localStorage.setItem('user', userJSON);
}

function clearLocalStorage() {
  localStorage.removeItem('accessToken');
  localStorage.removeItem('user');
}

function setAuthHeader(token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}

export default new Vuex.Store({
  state: {
    accessToken: null,
    user: null,
    loginErrors: { },
    registerErrors: { },
  },
  getters: {
    isAuthed(state) {
      return state.accessToken !== null;
    },
    user(state) {
      return state.user
    }
  },
  mutations: {
    login(state, data) {
      state.accessToken = data.accessToken;
      state.user = data.user;
    },
    logout(state) {
      state.accessToken = null;
      state.user = null;
    },
    setLoginErrors(state, data) {
      state.loginErrors = data;
    },
    clearLoginErrors(state) {
      state.loginErrors = { };
    },
    setRegisterErrors(state, data) {
      state.registerErrors = data;
    },
    clearRegisterErrors(state) {
      state.registerErrors = { };
    }
  },
  actions: {
    register({ commit }, payload) {
      axios.post('/register', {
        name: payload.name,
        password: payload.password
      })
        .then(res => {
          setAuthHeader(res.data.accessToken);
          const data = {
            accessToken: res.data.accessToken,
            user: res.data.user
          };
          commit('login', data);
          commit('clearRegisterErrors');
          setLocalStorage(data);
          router.replace('/dashboard');
        })
        .catch(err => {
          const errors = { };
          if (err.response) {
            switch(err.response.status) {
              case 409:
                errors.name = 'User with this name already exists.';
                break;
              default:
                errors.other = err.toString();
                break;
            }
          }
          else {
            errors.other = err.toString();
          }

          commit('setRegisterErrors', errors);
        });
    },

    login({ commit }, payload) {
      axios.post('/login', {
        name: payload.name,
        password: payload.password
      })
        .then(res => {
          setAuthHeader(res.data.accessToken);
          const data = {
            accessToken: res.data.accessToken,
            user: res.data.user
          }
          commit('login', data);
          commit('clearLoginErrors');
          setLocalStorage(data);
          router.replace('/dashboard');
        })
        .catch(err => {
          const errors = { };
          if (err.response) {
            switch(err.response.status) {
              case 401:
                errors.password = 'Invalid password.';
                break;
              case 404:
                errors.name = 'Invalid name.';
                break;
              default:
                errors.other = err.toString();
                break;
            }
          }
          else {
            errors.other = err.toString();
          }
          commit('setLoginErrors', errors);
        });
    },

    tryAutoLogin({ commit }) {
      const accessToken = localStorage.getItem('accessToken');
      const userJSON = localStorage.getItem('user');
      if (!accessToken || !userJSON) return;
      
      setAuthHeader(accessToken);
      const user = JSON.parse(userJSON);
      const data = {
        accessToken,
        user
      }
      commit('login', data);
      if (router.currentRoute.path !== '/')
        router.replace('/');
    },

    logout({ commit }) {
      setAuthHeader('');
      commit('logout');
      clearLocalStorage();
      router.replace('/login');
    }
  }
})