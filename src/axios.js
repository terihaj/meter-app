import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:24680'
});

export default instance;