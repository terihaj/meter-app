import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import Dashboard from './components/Dashboard';
import MeterList from './components/meters/MeterList';
import MeterDetails from './components/meters/MeterDetails';
import Meters from './components/meters/Meters';
import MeterAdd from './components/meters/MeterAdd';
import store from './store/store';

function beforeEnterAuthedCheck(to, from, next) {
  if (store.getters.isAuthed)
    next();
  else
    next('/login');
}

function beforeEnterNotAuthedCheck(to, from, next) {
  if (store.getters.isAuthed)
    next('/');
  else
    next();
}

const routes = [
  {
    path: '',
    component: Home
  },
  {
    name: 'login',
    path: '/login',
    component: Login,
    beforeEnter: beforeEnterNotAuthedCheck
  },
  {
    name: 'register',
    path: '/register',
    component: Register,
    beforeEnter: beforeEnterNotAuthedCheck
  },
  {
    path: '/dashboard',
    component: Dashboard,
    beforeEnter: beforeEnterAuthedCheck
  },
  {
    path: '/meters',
    component: Meters,
    beforeEnter: beforeEnterAuthedCheck,
    children: [
      {
        path: '',
        component: MeterList
      },
      {
        path: 'add',
        component: MeterAdd
      },
      {
        path: ':id',
        component: MeterDetails,
        name: 'meterDetails'
      }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
];

Vue.use(VueRouter);

export default new VueRouter({
  routes,
  mode: 'history'
});